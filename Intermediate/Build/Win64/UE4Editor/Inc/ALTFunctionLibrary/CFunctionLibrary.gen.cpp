// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/CFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCFunctionLibrary() {}
// Cross Module References
	ALTFUNCTIONLIBRARY_API UClass* Z_Construct_UClass_UCFunctionLibrary_NoRegister();
	ALTFUNCTIONLIBRARY_API UClass* Z_Construct_UClass_UCFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_ALTFunctionLibrary();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_Alphabetical();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_BooleanXNOR();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_ConcatInt();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_CountCharacterOccurences();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_FileLoadString();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_FileSaveString();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_GetAllSaveGameSlotNames();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_GetPermutations();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_MakeOperatorsTheSame();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_RoundFloat();
	ALTFUNCTIONLIBRARY_API UFunction* Z_Construct_UFunction_UCFunctionLibrary_TrimFull();
// End Cross Module References
	void UCFunctionLibrary::StaticRegisterNativesUCFunctionLibrary()
	{
		UClass* Class = UCFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Alphabetical", &UCFunctionLibrary::execAlphabetical },
			{ "BooleanXNOR", &UCFunctionLibrary::execBooleanXNOR },
			{ "ConcatInt", &UCFunctionLibrary::execConcatInt },
			{ "CountCharacterOccurences", &UCFunctionLibrary::execCountCharacterOccurences },
			{ "FileLoadString", &UCFunctionLibrary::execFileLoadString },
			{ "FileSaveString", &UCFunctionLibrary::execFileSaveString },
			{ "GetAllSaveGameSlotNames", &UCFunctionLibrary::execGetAllSaveGameSlotNames },
			{ "GetPermutations", &UCFunctionLibrary::execGetPermutations },
			{ "MakeOperatorsTheSame", &UCFunctionLibrary::execMakeOperatorsTheSame },
			{ "RoundFloat", &UCFunctionLibrary::execRoundFloat },
			{ "TrimFull", &UCFunctionLibrary::execTrimFull },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_Alphabetical()
	{
		struct CFunctionLibrary_eventAlphabetical_Parms
		{
			FString A;
			FString B;
			bool Equal;
			bool ABeforeB;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ABeforeB_SetBit = [](void* Obj){ ((CFunctionLibrary_eventAlphabetical_Parms*)Obj)->ABeforeB = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ABeforeB = { UE4CodeGen_Private::EPropertyClass::Bool, "ABeforeB", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000180, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(CFunctionLibrary_eventAlphabetical_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ABeforeB_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			auto NewProp_Equal_SetBit = [](void* Obj){ ((CFunctionLibrary_eventAlphabetical_Parms*)Obj)->Equal = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Equal = { UE4CodeGen_Private::EPropertyClass::Bool, "Equal", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000180, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(CFunctionLibrary_eventAlphabetical_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_Equal_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_B = { UE4CodeGen_Private::EPropertyClass::Str, "B", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventAlphabetical_Parms, B), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_A = { UE4CodeGen_Private::EPropertyClass::Str, "A", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventAlphabetical_Parms, A), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ABeforeB,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Equal,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_B,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_A,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Utilities|String" },
				{ "Keywords", "compare string lexicographical alphabetical order" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Return whether string A is equal to B and which should come first in alphabetical order" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "Alphabetical", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14442401, sizeof(CFunctionLibrary_eventAlphabetical_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_BooleanXNOR()
	{
		struct CFunctionLibrary_eventBooleanXNOR_Parms
		{
			bool A;
			bool B;
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((CFunctionLibrary_eventBooleanXNOR_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(CFunctionLibrary_eventBooleanXNOR_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			auto NewProp_B_SetBit = [](void* Obj){ ((CFunctionLibrary_eventBooleanXNOR_Parms*)Obj)->B = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_B = { UE4CodeGen_Private::EPropertyClass::Bool, "B", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(CFunctionLibrary_eventBooleanXNOR_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_B_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			auto NewProp_A_SetBit = [](void* Obj){ ((CFunctionLibrary_eventBooleanXNOR_Parms*)Obj)->A = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_A = { UE4CodeGen_Private::EPropertyClass::Bool, "A", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(CFunctionLibrary_eventBooleanXNOR_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_A_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_B,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_A,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Math|Boolean" },
				{ "CompactNodeTitle", "XNOR" },
				{ "DisplayName", "XNOR Boolean" },
				{ "Keywords", "^ xnor" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Returns the logical eXclusive NOR of two values (A XNOR B)" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "BooleanXNOR", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14042401, sizeof(CFunctionLibrary_eventBooleanXNOR_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_ConcatInt()
	{
		struct CFunctionLibrary_eventConcatInt_Parms
		{
			int32 A;
			int32 B;
			int32 ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventConcatInt_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_B = { UE4CodeGen_Private::EPropertyClass::Int, "B", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventConcatInt_Parms, B), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_A = { UE4CodeGen_Private::EPropertyClass::Int, "A", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventConcatInt_Parms, A), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_B,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_A,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Math|Int" },
				{ "DisplayName", "Append Int" },
				{ "Keywords", "concat int append" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Return the concatenation of A and B" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "ConcatInt", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14042401, sizeof(CFunctionLibrary_eventConcatInt_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_CountCharacterOccurences()
	{
		struct CFunctionLibrary_eventCountCharacterOccurences_Parms
		{
			FString SourceString;
			FString Character;
			int32 ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventCountCharacterOccurences_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Character = { UE4CodeGen_Private::EPropertyClass::Str, "Character", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventCountCharacterOccurences_Parms, Character), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_SourceString = { UE4CodeGen_Private::EPropertyClass::Str, "SourceString", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventCountCharacterOccurences_Parms, SourceString), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Character,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_SourceString,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "String" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Returns how many characters occur in the source string." },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "CountCharacterOccurences", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14042401, sizeof(CFunctionLibrary_eventCountCharacterOccurences_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_FileLoadString()
	{
		struct CFunctionLibrary_eventFileLoadString_Parms
		{
			FString FileName;
			FString SaveText;
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((CFunctionLibrary_eventFileLoadString_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(CFunctionLibrary_eventFileLoadString_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_SaveText = { UE4CodeGen_Private::EPropertyClass::Str, "SaveText", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000180, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventFileLoadString_Parms, SaveText), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName = { UE4CodeGen_Private::EPropertyClass::Str, "FileName", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventFileLoadString_Parms, FileName), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_SaveText,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FileName,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Saving" },
				{ "Keywords", "file load string" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Returns a string from a file." },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "FileLoadString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14442401, sizeof(CFunctionLibrary_eventFileLoadString_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_FileSaveString()
	{
		struct CFunctionLibrary_eventFileSaveString_Parms
		{
			FString SaveText;
			FString FileName;
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((CFunctionLibrary_eventFileSaveString_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(CFunctionLibrary_eventFileSaveString_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName = { UE4CodeGen_Private::EPropertyClass::Str, "FileName", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventFileSaveString_Parms, FileName), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_SaveText = { UE4CodeGen_Private::EPropertyClass::Str, "SaveText", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventFileSaveString_Parms, SaveText), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FileName,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_SaveText,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Saving" },
				{ "Keywords", "file save string" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Saves a string to a file." },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "FileSaveString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04042401, sizeof(CFunctionLibrary_eventFileSaveString_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_GetAllSaveGameSlotNames()
	{
		struct CFunctionLibrary_eventGetAllSaveGameSlotNames_Parms
		{
			TArray<FString> ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Array, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventGetAllSaveGameSlotNames_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue_Inner,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Saving" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Returns all save game file names from the Saved folder." },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "GetAllSaveGameSlotNames", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14042401, sizeof(CFunctionLibrary_eventGetAllSaveGameSlotNames_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_GetPermutations()
	{
		struct CFunctionLibrary_eventGetPermutations_Parms
		{
			FString A;
			TArray<FString> ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Array, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventGetPermutations_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_A = { UE4CodeGen_Private::EPropertyClass::Str, "A", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventGetPermutations_Parms, A), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_A,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Utilities|String" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Return all permutations of A (Max characters is 9)" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "GetPermutations", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14042401, sizeof(CFunctionLibrary_eventGetPermutations_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_MakeOperatorsTheSame()
	{
		struct CFunctionLibrary_eventMakeOperatorsTheSame_Parms
		{
			float OperatorToChange;
			float OperatorBase;
			float ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventMakeOperatorsTheSame_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OperatorBase = { UE4CodeGen_Private::EPropertyClass::Float, "OperatorBase", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventMakeOperatorsTheSame_Parms, OperatorBase), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OperatorToChange = { UE4CodeGen_Private::EPropertyClass::Float, "OperatorToChange", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventMakeOperatorsTheSame_Parms, OperatorToChange), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_OperatorBase,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_OperatorToChange,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Math|Float" },
				{ "CompactNodeTitle", "SameSigns" },
				{ "DisplayName", "Make Operators The Same" },
				{ "Keywords", "operator sign same change" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Returns the OperatorToChange being the same sign as the OperatorBase" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "MakeOperatorsTheSame", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14042401, sizeof(CFunctionLibrary_eventMakeOperatorsTheSame_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_RoundFloat()
	{
		struct CFunctionLibrary_eventRoundFloat_Parms
		{
			float A;
			int32 B;
			float ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventRoundFloat_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_B = { UE4CodeGen_Private::EPropertyClass::Int, "B", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventRoundFloat_Parms, B), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_A = { UE4CodeGen_Private::EPropertyClass::Float, "A", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventRoundFloat_Parms, A), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_B,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_A,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Math|Float" },
				{ "CompactNodeTitle", "Round" },
				{ "DisplayName", "Round To Decimal Point" },
				{ "Keywords", ". round decimal point" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Returns the float value rounded to the specified demical place" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "RoundFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14042401, sizeof(CFunctionLibrary_eventRoundFloat_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UCFunctionLibrary_TrimFull()
	{
		struct CFunctionLibrary_eventTrimFull_Parms
		{
			FString A;
			FString ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventTrimFull_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_A = { UE4CodeGen_Private::EPropertyClass::Str, "A", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(CFunctionLibrary_eventTrimFull_Parms, A), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_A,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Utilities|String" },
				{ "Keywords", "trim full whitespace string" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
				{ "ToolTip", "Return the input string trimmed of all whitespace" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UCFunctionLibrary, "TrimFull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14042401, sizeof(CFunctionLibrary_eventTrimFull_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCFunctionLibrary_NoRegister()
	{
		return UCFunctionLibrary::StaticClass();
	}
	UClass* Z_Construct_UClass_UCFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
				(UObject* (*)())Z_Construct_UPackage__Script_ALTFunctionLibrary,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_UCFunctionLibrary_Alphabetical, "Alphabetical" }, // 2284848658
				{ &Z_Construct_UFunction_UCFunctionLibrary_BooleanXNOR, "BooleanXNOR" }, // 3504128027
				{ &Z_Construct_UFunction_UCFunctionLibrary_ConcatInt, "ConcatInt" }, // 1087946693
				{ &Z_Construct_UFunction_UCFunctionLibrary_CountCharacterOccurences, "CountCharacterOccurences" }, // 425668611
				{ &Z_Construct_UFunction_UCFunctionLibrary_FileLoadString, "FileLoadString" }, // 1139376790
				{ &Z_Construct_UFunction_UCFunctionLibrary_FileSaveString, "FileSaveString" }, // 2495463504
				{ &Z_Construct_UFunction_UCFunctionLibrary_GetAllSaveGameSlotNames, "GetAllSaveGameSlotNames" }, // 441614786
				{ &Z_Construct_UFunction_UCFunctionLibrary_GetPermutations, "GetPermutations" }, // 2579521216
				{ &Z_Construct_UFunction_UCFunctionLibrary_MakeOperatorsTheSame, "MakeOperatorsTheSame" }, // 410536009
				{ &Z_Construct_UFunction_UCFunctionLibrary_RoundFloat, "RoundFloat" }, // 3968180604
				{ &Z_Construct_UFunction_UCFunctionLibrary_TrimFull, "TrimFull" }, // 1407261175
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "CFunctionLibrary.h" },
				{ "ModuleRelativePath", "Public/CFunctionLibrary.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<UCFunctionLibrary>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&UCFunctionLibrary::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00100080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCFunctionLibrary, 26470937);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCFunctionLibrary(Z_Construct_UClass_UCFunctionLibrary, &UCFunctionLibrary::StaticClass, TEXT("/Script/ALTFunctionLibrary"), TEXT("UCFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
