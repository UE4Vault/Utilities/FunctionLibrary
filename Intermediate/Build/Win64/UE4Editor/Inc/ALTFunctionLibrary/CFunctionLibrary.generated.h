// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ALTFUNCTIONLIBRARY_CFunctionLibrary_generated_h
#error "CFunctionLibrary.generated.h already included, missing '#pragma once' in CFunctionLibrary.h"
#endif
#define ALTFUNCTIONLIBRARY_CFunctionLibrary_generated_h

#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAlphabetical) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_A); \
		P_GET_PROPERTY(UStrProperty,Z_Param_B); \
		P_GET_UBOOL_REF(Z_Param_Out_Equal); \
		P_GET_UBOOL_REF(Z_Param_Out_ABeforeB); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UCFunctionLibrary::Alphabetical(Z_Param_A,Z_Param_B,Z_Param_Out_Equal,Z_Param_Out_ABeforeB); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTrimFull) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_A); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UCFunctionLibrary::TrimFull(Z_Param_A); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetPermutations) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_A); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=UCFunctionLibrary::GetPermutations(Z_Param_A); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConcatInt) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_A); \
		P_GET_PROPERTY(UIntProperty,Z_Param_B); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=UCFunctionLibrary::ConcatInt(Z_Param_A,Z_Param_B); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCountCharacterOccurences) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SourceString); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Character); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=UCFunctionLibrary::CountCharacterOccurences(Z_Param_SourceString,Z_Param_Character); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFileLoadString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_SaveText); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UCFunctionLibrary::FileLoadString(Z_Param_FileName,Z_Param_Out_SaveText); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFileSaveString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SaveText); \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UCFunctionLibrary::FileSaveString(Z_Param_SaveText,Z_Param_FileName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetAllSaveGameSlotNames) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=UCFunctionLibrary::GetAllSaveGameSlotNames(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMakeOperatorsTheSame) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_OperatorToChange); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_OperatorBase); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=UCFunctionLibrary::MakeOperatorsTheSame(Z_Param_OperatorToChange,Z_Param_OperatorBase); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRoundFloat) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_A); \
		P_GET_PROPERTY(UIntProperty,Z_Param_B); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=UCFunctionLibrary::RoundFloat(Z_Param_A,Z_Param_B); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBooleanXNOR) \
	{ \
		P_GET_UBOOL(Z_Param_A); \
		P_GET_UBOOL(Z_Param_B); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UCFunctionLibrary::BooleanXNOR(Z_Param_A,Z_Param_B); \
		P_NATIVE_END; \
	}


#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAlphabetical) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_A); \
		P_GET_PROPERTY(UStrProperty,Z_Param_B); \
		P_GET_UBOOL_REF(Z_Param_Out_Equal); \
		P_GET_UBOOL_REF(Z_Param_Out_ABeforeB); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UCFunctionLibrary::Alphabetical(Z_Param_A,Z_Param_B,Z_Param_Out_Equal,Z_Param_Out_ABeforeB); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTrimFull) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_A); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UCFunctionLibrary::TrimFull(Z_Param_A); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetPermutations) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_A); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=UCFunctionLibrary::GetPermutations(Z_Param_A); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConcatInt) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_A); \
		P_GET_PROPERTY(UIntProperty,Z_Param_B); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=UCFunctionLibrary::ConcatInt(Z_Param_A,Z_Param_B); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCountCharacterOccurences) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SourceString); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Character); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=UCFunctionLibrary::CountCharacterOccurences(Z_Param_SourceString,Z_Param_Character); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFileLoadString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_SaveText); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UCFunctionLibrary::FileLoadString(Z_Param_FileName,Z_Param_Out_SaveText); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFileSaveString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SaveText); \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UCFunctionLibrary::FileSaveString(Z_Param_SaveText,Z_Param_FileName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetAllSaveGameSlotNames) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=UCFunctionLibrary::GetAllSaveGameSlotNames(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMakeOperatorsTheSame) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_OperatorToChange); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_OperatorBase); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=UCFunctionLibrary::MakeOperatorsTheSame(Z_Param_OperatorToChange,Z_Param_OperatorBase); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRoundFloat) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_A); \
		P_GET_PROPERTY(UIntProperty,Z_Param_B); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=UCFunctionLibrary::RoundFloat(Z_Param_A,Z_Param_B); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execBooleanXNOR) \
	{ \
		P_GET_UBOOL(Z_Param_A); \
		P_GET_UBOOL(Z_Param_B); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UCFunctionLibrary::BooleanXNOR(Z_Param_A,Z_Param_B); \
		P_NATIVE_END; \
	}


#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCFunctionLibrary(); \
	friend ALTFUNCTIONLIBRARY_API class UClass* Z_Construct_UClass_UCFunctionLibrary(); \
public: \
	DECLARE_CLASS(UCFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ALTFunctionLibrary"), NO_API) \
	DECLARE_SERIALIZER(UCFunctionLibrary) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCFunctionLibrary(); \
	friend ALTFUNCTIONLIBRARY_API class UClass* Z_Construct_UClass_UCFunctionLibrary(); \
public: \
	DECLARE_CLASS(UCFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ALTFunctionLibrary"), NO_API) \
	DECLARE_SERIALIZER(UCFunctionLibrary) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCFunctionLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCFunctionLibrary(UCFunctionLibrary&&); \
	NO_API UCFunctionLibrary(const UCFunctionLibrary&); \
public:


#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCFunctionLibrary(UCFunctionLibrary&&); \
	NO_API UCFunctionLibrary(const UCFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCFunctionLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCFunctionLibrary)


#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_12_PROLOG
#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_RPC_WRAPPERS \
	HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_INCLASS \
	HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_ALTFunctionLibrary_Source_ALTFunctionLibrary_Public_CFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
