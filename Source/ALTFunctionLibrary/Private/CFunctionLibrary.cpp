// Fill out your copyright notice in the Description page of Project Settings.

#include "CFunctionLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "PlatformFeatures.h"
#include "GameFramework/SaveGame.h"
#include <iostream>
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/GameplayStatics.h"
#include <windows.h>

bool UCFunctionLibrary::BooleanXNOR(bool A, bool B)
{
	return !(A ^ B);
}

float UCFunctionLibrary::RoundFloat(float A, int32 B)
{
	return FMath::FloorToFloat(A*B) / B;
}

float UCFunctionLibrary::MakeOperatorsTheSame(float OperatorToChange, float OperatorBase)
{
	return UKismetMathLibrary::SelectFloat(OperatorToChange, OperatorToChange*-1, BooleanXNOR(OperatorToChange < 0, OperatorBase < 0));
}

TArray<FString> UCFunctionLibrary::GetAllSaveGameSlotNames()
{
	TArray<FString> ret;
	ISaveGameSystem* SaveSystem = IPlatformFeaturesModule::Get().GetSaveGameSystem();

	// If we have a save system and a valid name..
	if (SaveSystem)
	{
		// From SaveGameSystem.h in the Unreal source code base.
		FString saveGamePath = FString::Printf(TEXT("%s/SaveGames/*"), *FPaths::ProjectSavedDir());

		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Search path %s"), *saveGamePath);

		WIN32_FIND_DATA fd;
		HANDLE hFind = ::FindFirstFile(*saveGamePath, &fd);
		if (hFind != INVALID_HANDLE_VALUE) {
			do {
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("  Save Name: %s"), fd.cFileName);
				// Disallow empty names and . and .. and don't get all tied up in WCHAR issues.
				if (fd.cFileName[0] == '\0' ||
					fd.cFileName[0] == '.' && fd.cFileName[1] == '\0' ||
					fd.cFileName[0] == '.' && fd.cFileName[1] == '.' && fd.cFileName[2] == '\0')
				{
				}
				//Check if: This is a directory && The first letter of the file name contains a "." && File name contains .sav && Is not the MainMenu.sav
				else if ((FILE_ATTRIBUTE_DIRECTORY) != 0 && fd.cFileName[0] != '.' && FString(fd.cFileName).Contains(".sav") == true && FString(fd.cFileName).Equals("MainMenu.sav") == false) {
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Adding File!"));
					ret.Add(FString(fd.cFileName).Replace(TEXT(".sav"), TEXT("")));
				}
			} while (::FindNextFile(hFind, &fd));
			::FindClose(hFind);
		}
	}

	return ret;
}

bool UCFunctionLibrary::FileSaveString(FString SaveText, FString FileName)
{
	return FFileHelper::SaveStringToFile(SaveText, *(FPaths::GameDir() + FileName));
}

bool UCFunctionLibrary::FileLoadString(FString FileName, FString& SaveText)
{
	return FFileHelper::LoadFileToString(SaveText, *(FPaths::GameDir() + FileName));
}

int32 UCFunctionLibrary::CountCharacterOccurences(FString SourceString, FString Character) {
	int32 counter = 0;
	//Loop through source string, start to finish
	for (int32 i = 0; i < SourceString.Len(); i++) {
		//If character is a LINE_TERMINATOR (\r\n) replace it with new line (\n) for checking purposes
		if (Character.Equals(LINE_TERMINATOR))
			Character = "\n";
		//If characters are equal add 1 to character count
		if (UKismetStringLibrary::GetSubstring(SourceString, i).Equals(Character)) {
			counter++;
		}
	}
	//Return character count when finished
	return counter;
}

int32 UCFunctionLibrary::ConcatInt(int32 A, int32 B)
{
	int32 pow = 10;
	while (B >= pow)
		pow *= 10;
	return A * pow + B;
}

TArray<FString> UCFunctionLibrary::GetPermutations(FString A)
{
	//Max characters before lag occurs is 9 so if string is greater, set to 9
	A = A.Mid(0, 9);
	TArray<FString> ret;
	TArray<int32> factorials;
	factorials.SetNum(A.Len() + 1);
	factorials[0] = 1;
	for (int32 i = 1; i <= A.Len(); i++) {
		factorials[i] = factorials[i - 1] * i;
	}

	for (int32 i = 0; i < factorials[A.Len()]; i++) {
		FString onePermutation = "";
		FString temp = A;
		int32 positionCode = i;
		for (int32 position = A.Len(); position > 0; position--) {
			int32 selected = positionCode / factorials[position - 1];
			onePermutation += temp.Mid(selected, 1);
			positionCode = positionCode % factorials[position - 1];
			temp = temp.Mid(0, selected) + temp.Mid(selected + 1);
		}
		ret.Add(onePermutation);
	}
	return ret;
}

FString UCFunctionLibrary::TrimFull(FString A)
{
	TArray<FString> LocalA = UKismetStringLibrary::GetCharacterArrayFromString(A);
	FString LocalTrimmedString = "";
	for (int32 i = 0; i < LocalA.Num(); i++)
		if (UKismetStringLibrary::NotEqual_StrStr(LocalA[i], " "))
		{
			LocalTrimmedString = UKismetStringLibrary::Concat_StrStr(LocalTrimmedString, LocalA[i]);
		}
	return LocalTrimmedString;
}

void UCFunctionLibrary::Alphabetical(FString A, FString B, bool& Equal, bool& ABeforeB)
{
	if (UKismetStringLibrary::EqualEqual_StrStr(A, B))
	{
		Equal = true;
		ABeforeB = false;
	}
	TArray<FString> LocalACharacters = UKismetStringLibrary::GetCharacterArrayFromString(A);
	TArray<FString> LocalBCharacters = UKismetStringLibrary::GetCharacterArrayFromString(B);
	bool AIsShorter = false;
	AIsShorter = LocalACharacters.Num() <= LocalBCharacters.Num();
	TArray<FString> Selection;
	TArray<FString> NotSelection;
	bool LocalABeforeB = false;
	bool BreakOut = false;
	if (AIsShorter)
	{
		Selection = LocalACharacters;
		NotSelection = LocalBCharacters;
	}
	else
	{
		Selection = LocalBCharacters;
		NotSelection = LocalACharacters;
	}
	for (int32 i = 0; i<Selection.Num(); i++)
	{
		if (!BreakOut)
		{
			if (UKismetStringLibrary::GetCharacterAsNumber(Selection[i], 0) > UKismetStringLibrary::GetCharacterAsNumber(NotSelection[i], 0))
			{
				LocalABeforeB = !AIsShorter;
				BreakOut = true;
			}
			else if (UKismetStringLibrary::GetCharacterAsNumber(Selection[i], 0) < UKismetStringLibrary::GetCharacterAsNumber(NotSelection[i], 0))
			{
				LocalABeforeB = AIsShorter;
				BreakOut = true;
			}
		}
	}
	if (BreakOut)
	{
		Equal = false;
		ABeforeB = LocalABeforeB;
	}
	else
	{
		Equal = false;
		ABeforeB = AIsShorter;
	}
}