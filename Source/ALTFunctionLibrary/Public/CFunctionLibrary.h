// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class ALTFUNCTIONLIBRARY_API UCFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	/* Returns the logical eXclusive NOR of two values (A XNOR B) */
	UFUNCTION(BlueprintPure, meta = (DisplayName = "XNOR Boolean", CompactNodeTitle = "XNOR", Keywords = "^ xnor"), Category = "Math|Boolean")
		static bool BooleanXNOR(bool A, bool B);

	/*Returns the float value rounded to the specified demical place*/
	UFUNCTION(BlueprintPure, meta = (DisplayName = "Round To Decimal Point", CompactNodeTitle = "Round", Keywords = ". round decimal point"), Category = "Math|Float")
		static float RoundFloat(float A, int32 B);

	/*Returns the OperatorToChange being the same sign as the OperatorBase*/
	UFUNCTION(BlueprintPure, meta = (DisplayName = "Make Operators The Same", CompactNodeTitle = "SameSigns", Keywords = "operator sign same change"), Category = "Math|Float")
		static float MakeOperatorsTheSame(float OperatorToChange, float OperatorBase);

	/*Returns all save game file names from the Saved folder.*/
	UFUNCTION(BlueprintPure, Category = "Saving")
		static TArray<FString> GetAllSaveGameSlotNames();

	/*Saves a string to a file.*/
	UFUNCTION(BlueprintCallable, meta = (Keywords = "file save string", Category = "Saving"))
		static bool FileSaveString(FString SaveText, FString FileName);

	/*Returns a string from a file.*/
	UFUNCTION(BlueprintPure, meta = (Keywords = "file load string", Category = "Saving"))
		static bool FileLoadString(FString FileName, FString& SaveText);

	/*Returns how many characters occur in the source string.*/
	UFUNCTION(BlueprintPure, Category = "String")
		static int32 CountCharacterOccurences(FString SourceString, FString Character);

	/*Return the concatenation of A and B*/
	UFUNCTION(BlueprintPure, meta = (DisplayName = "Append Int", Keywords = "concat int append", Category = "Math|Int"))
		static int32 ConcatInt(int32 A, int32 B);

	/*Return all permutations of A (Max characters is 9)*/
	UFUNCTION(BlueprintPure, Category = "Utilities|String")
		static TArray<FString> GetPermutations(FString A);

	/*Return the input string trimmed of all whitespace*/
	UFUNCTION(BlueprintPure, meta = (Keywords = "trim full whitespace string", Category = "Utilities|String"))
		static FString TrimFull(FString A);

	/*Return whether string A is equal to B and which should come first in alphabetical order*/
	UFUNCTION(BlueprintPure, meta = (Keywords = "compare string lexicographical alphabetical order", Category = "Utilities|String"))
		static void Alphabetical(FString A, FString B, bool& Equal, bool& ABeforeB);
};